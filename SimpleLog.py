#! python3

import sys, pickle
import ctypes
ctypes.windll.kernel32.SetConsoleTitleA(b"Driving Log")
dataList = []
helplist = ["Here is a list of all the commands accepted by the software:",
            "add\t# Add a new entry to the database",
            "remove\t# Remove an entry from the database according to its index",
            "list\t# Show the database",
            "help\t# Show this list",
            "exit\t# Exit the program"]
try:
    fileb = open("lokiData.txt", "rb")
except FileNotFoundError:
    fileb = open("lokiData.txt", "a")
    fileb = open("lokiData.txt", "rb")

def loadOldData():
    global dataList, fileb
    try:
        dataLoad = pickle.load(fileb)
        for i in dataLoad:
            dataList.append(i)
    except EOFError:
        print("Database is empty. Please add something there!")

def addNewData():
    newData = input("Insert data here: ")
    global dataList
    fileb = open("lokiData.txt", "wb")
    if newData.strip() != "":
        dataList.append(newData)
        for i in dataList:
            pickle.dump(dataList, fileb)
        print("New data added to log.")
    else:
        print("Entry cancelled.")

def removeData():
    try:
        rmv = int(input("Index of entry being removed: "))
        global dataList
        if rmv > len(dataList):
            print("That entry does not exist.")
        else:
            global dataList
            fileb = open("lokiData.txt", "wb")
            rmv = rmv -1
            entry = dataList[rmv]
            del dataList[rmv]
            for i in dataList:
                pickle.dump(dataList, fileb)
            print("Entry \"", entry, "\" has been removed succesfully!", sep="")
    except ValueError:
        print("Cancelled.")

def listData():
    global dataList
    for i in dataList:
        print(dataList.index(i) + 1, "# ", i, sep="")
    print("\nThe log has ", len(dataList), " entries in total.", sep="")

loadOldData()
print("Welcome! For a list of commands, type help.")
while True:
    menu = input(">>> ")
    if menu.lower() == "add":
        addNewData()

    elif menu.lower() == "remove":
        removeData()

    elif menu.lower() == "list":
        listData()

    elif menu.lower() == "help":
        for i in helplist:
            print(i)

    elif menu.lower() == "exit":
        sys.exit("Exiting program...")

    elif menu.lower().strip() == "":
        print("No command entered!")

    else:
        print("Command not found.")
